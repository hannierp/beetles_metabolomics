# Beneficial rhizobacteria and virus infection modulate the soybean metabolome and influence the feeding preferences of the virus vector *Epilachna varivestis*

This repository contains the data and source code to reproduce the main results of our bioRxiv preprint. For more information please refer to:
Beneficial rhizobacteria and virus infection modulate the soybean metabolome and influence the feeding preferences of the virus vector *Epilachna varivestis*
Hannier Pulido, Kerry E. Mauck, Consuelo M. De Moraes, Mark C. Mescher. bioRxiv preprint (2024)

The implementation is written in R.

*Correspondence and requests for materials should be addressed to Mark C. Mescher, e-mail: mescher@usys.ethz.ch


# Hardware Requirements

To successfully run the code in this repository, it is recommended to have a computer with sufficient hardware specifications. The code relies on in-memory operations, so a system with at least 16GB of RAM is recommended. Part of this code was run on on a workstation equipped with an AMD Ryzen 9 7950X 4.5GHz Processor.
We used `Parallel Processing` and the `ETH Euler computer` to run the recursive feature elimination models.


# Files

This repository contains the following code files:

1. `counts_wgcna.R`: This code runs WGCNA on the whole transcriptomics dataset
2. `metabolites_limma.R`: linear analysis using the limma package to compare different treatments on metabolomics data 
3. `metabolites_rfe.R`: This code runs multiple recursive feature elimination models (rf, AdaBag, svmRadial) on the metabolomics dataset
4. `counts_deseq.R`: Differential expression analysis using the DESeq2
5. `counts_gage.R`: This code generate a list containing the significant pathways for the different contrasts described in the supplementary Table S2.2.
6. `counts_rfe.R`: This code runs multiple recursive feature elimination models (rf, AdaBag, svmRadial) on the transcriptomics dataset
7. `behavioral_assays.R`:  Statistical analysis for behavioral assays depicted in Figures 2-4
8. `plant_traits.R`: Plant physical traits analysis for data depicted in Figure 5

