# Warning: This code runs WGCNA on the whole transcriptomics dataset and
# is a computationally intensive job that requires significant RAM and
# processing power. This code was run on the ETH cluster computer with
# 48 parallel threads. Please ensure that your computer or cluster
# can handle the resource requirements before running this code.

# Source code to generate the Supplementary file S5.3. “Supplementary_file_S5.3_wgcna_gene_to_module_membership_20230413.xlsx”

# Load functions
source("Functions_OmicsData.R")

# load libraries
library(WGCNA)
library(dplyr)
library(ggplot2)
library(limma)
library(shiny)
library(openxlsx)
library(viridis)
library(gridExtra)
library(ggpubr)
library(igraph)
library(tidygraph)
library(ggraph)
library(RCy3)

# Load data
load("Pulido_et_al_2023_OmicsData.RData")

# Filter genes with too many missing entries
goodg <- goodGenes(normalizedCounts[, 5:ncol(normalizedCounts)], verbose = 10)
datExpr0 <- normalizedCounts[, goodg]
rownames(datExpr0) <- normalizedCounts$Row.names

# Determine WGCNA parameters
sft <- pickSoftThreshold(
        datExpr0,
        dataIsExpr = TRUE,
        corFnc = cor,
        networkType = "signed"
)

sft_df <- data.frame(sft$fitIndices) %>%
        dplyr::mutate(model_fit = -sign(slope) * SFT.R.sq)


ggplot(sft_df, aes(x = Power, y = model_fit, label = Power)) +
        geom_point() +
        # We'll put the Power labels slightly above the data points
geom_text(nudge_y = 0.1) +
        # We will plot what WGCNA recommends as an R^2 cutoff
        geom_hline(yintercept = 0.80, col = "red") +
        ylim(c(min(sft_df$model_fit), 1.05)) +
        xlab("Soft Threshold (power)") +
        ylab("Scale Free Topology Model Fit, signed R^2") +
        ggtitle("Scale independence") +
        theme_classic()
# From this plot, I select a power = 12 for a model fit of 0.83

# Generate the network object
bwnet.all <- blockwiseModules(
        datExpr0,
        maxBlockSize = 50000,
        TOMType = "signed",
        networkType = "signed",
        power = 12,
        numericLabels = TRUE,
        randomSeed = 1234,
        saveTOMs = TRUE,
        saveTOMFileBase = "blockwiseTOM",
        nThreads = 48
)
TOM.all <- TOMsimilarityFromExpr(
        datExpr0,
        power = 10,
        networkType = "signed",
        TOMType = "signed",
        nThreads = 48
)


# Extract the eigenvectors of each gene module. Eigenvectors are summary
# measures of gene expression patterns within each module, and can be used to
# identify hub genes that play important roles in the module.
module_eigengenes <- bwnet.all$MEs

# Add back row.names to module_eigengenes
rownames(module_eigengenes) <- normalizedCounts$Row.names

# Merge similar modules
# It reduces the number of modules from 59 to 49.
## Clustering of module eigengenes
module_eigengenes <- bwnet.all$MEs
WMEDiss <- 1 - cor(module_eigengenes)
WMETree  <- hclust(as.dist(WMEDiss), method = "average")
MEDissThres  <- 0.30

# Plot cluster of modules and check threshold for merging
plot(WMETree,
     main = "Clustering of module eigengenes",
     xlab = "",
     sub = "")
abline(h = MEDissThres, col = "red")

# Merge modules
merge  <-
        mergeCloseModules(datExpr0,
                          (bwnet.all$colors),
                          cutHeight = MEDissThres,
                          verbose = 3)


# Overwrite module eigengenes object
module_eigengenes <- merge$newMEs

# Plot Gene dendrogram and module colors, Figure 8a
consTree <- hclust(as.dist(1 - TOM), method = "average")
plotDendroAndColors(
        consTree,
        labels2colors(merge$colors),
        dendroLabels = FALSE,
        hang = 0.03,
        addGuide = TRUE,
        guideHang = 0.05,
        main = "Gene dendrogram and module membership"
)

# Get corresponence of module number with module color according to WGCNA
ME.colors <- data.frame(ME = merge$colors, ME.color = labels2colors(merge$colors))
ME.colors %>% count(ME, ME.color)


# Linear models to test module differences between treatment groups
# This approach will highlight the modules to focus for the network and
# identifying hub genes important for those comparisons.

## Confirm samples are in order
all.equal(normalizedCounts$Row.names, rownames(module_eigengenes))

## Extract metadata from counts and generate rhizo.virus factor
norm.metadata <- normalizedCounts[, 1:4]

## Create the design matrix from rhi.virus treatment
des_mat <- model.matrix(~ norm.metadata$group)
colnames(des_mat) <-
        c(
                "control.uninfected",
                "control.infected",
                "Bj.infected",
                "Bj.uninfected",
                "BjDa.infected",
                "BjDa.uninfected",
                "Da.infected",
                "Da.uninfected"
        )

## Generate limma objects using contrasts
cm <- makeContrasts(
        (Bj.uninfected + BjDa.uninfected + Da.uninfected) / 3 - control.uninfected,
        (Bj.infected + BjDa.infected + Da.infected) / 3 - control.uninfected,
        (Bj.infected + BjDa.infected + Da.infected) / 3 - (Bj.uninfected + BjDa.uninfected + Da.uninfected) / 3,
        levels = des_mat
)

fitx <- lmFit(t(module_eigengenes), design = des_mat)
fitx <- contrasts.fit(fitx, cm)
fitx <- eBayes(fitx)


## Generate the color-module dataframes
stats_df.res.g <- get_ME_colors_count(1) %>% mutate(comparison_ID = "res.g.mixed")
stats_df.res.h <- get_ME_colors_count(2) %>% mutate(comparison_ID = "res.h.mixed")
stats_df.res.i <- get_ME_colors_count(3) %>% mutate(comparison_ID = "res.i.mixed")

## Combine the dataframes 
wgcna.limma.results <- rbind(stats_df.res.g, stats_df.res.h, stats_df.res.i) %>%
        mutate(treatment1 = case_when(
                comparison_ID == "res.g.mixed" ~ "allBacteria.uninfected",
                comparison_ID == "res.h.mixed" ~ "allBacteria.infected",
                comparison_ID == "res.i.mixed" ~ "allBacteria.infected"
        )) %>%
        mutate(treatment2 = case_when(
                comparison_ID == "res.g.mixed" ~ "control.uninfected",
                comparison_ID == "res.h.mixed" ~ "control.uninfected",
                comparison_ID == "res.i.mixed" ~ "allBacteria.uninfected"
        )) %>%
        dplyr::rename(id = "comparison_ID") %>%
        select(id, treatment1, treatment2, module, ME.color, n, logFC, t, P.Value, adj.P.Val)

## Write Supplementary file S4.4. “results_limma_wgcna_20230413.txt”
write.table(wgcna.limma.results, file = "results_limma_wgcna_20230413.txt", sep = "\t", row.names = FALSE)


# Get gene module membership
c_modules <- data.frame(merge$colors)
row.names(c_modules) <- colnames(datExpr0)

module.list.set1 <- substring(colnames(module_eigengenes), 3)
index.set1 <- 0

Network <- list()
for (i in 1:length(module.list.set1)) {
        index.set1 <- which(c_modules == module.list.set1[i])
        Network[[i]] <- row.names(c_modules)[index.set1]
}
names(Network) <- module.list.set1

# Calculate module membership
modulekME <- signedKME(datExpr0, merge$newMEs)

# Get the hubgenes in a list
hubgenes <- lapply(seq_len(length(Network)), function(x) {
        dat <- modulekME[Network[[x]], ]
        dat <- dat[order(-dat[paste0("kME", names(Network)[x])]), ]
        gene <- data.frame(gene = rownames(dat), kme = dat[, 1])
        gene <- rownames(dat)
        return(gene)
})
names(hubgenes) <- names(Network)


# Get the hubgenes in a dataframe ordered by gene importance per module
d <- data.frame(gene = unlist(hubgenes),
                modulePosition = names(unlist(hubgenes)),
                module = rep(names(hubgenes), lengths(hubgenes)), 
                position = unlist(purrr::map(hubgenes, seq_along))
                )


# add more info about the entrez_ID and genename
d.geneNames <- left_join(d,
                         entrez_more_info,
                         by = c("gene" = "Gene")) %>% 
        select(gene, Entrez_id, symbol, genename, module, position) %>% 
        arrange(as.numeric(module), position)

# Write table
write.table(d.geneNames, file = "wgcna_gene_to_module_membership_20230413.txt", sep = "\t", row.names = FALSE)


# End of the run ##
